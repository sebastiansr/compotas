import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class Productos {

    private int idProducto;
    private String Descripcion;
    private String ClaveP;
    private String CodigoBarra;  
    
    public Productos() {

        idProducto = 0;
        Descripcion = "";
        ClaveP = "";
        CodigoBarra = "";
        
    }

    public int getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(int ValoridProducto) {
        this.idProducto = ValoridProducto;
    }

    public String getDescripcion() {
        return Descripcion;
    }

    public void setDescripcion(String ValorDescripcion) {
        this.Descripcion = ValorDescripcion;
    }

    public String getClaveP() {
        return ClaveP;
    }

    public void setClaveP(String ValorClaveP) {
        this.ClaveP = ValorClaveP;
    }

    public String getCodigoBarra() {
        return CodigoBarra;
    }

    public void setCodigoBarra(String ValorCodigoBarra) {
        this.CodigoBarra = ValorCodigoBarra;
    }
    
        public void setInsertarProducto(int ValidProducto, String ValDescripcion, String ValClaveP, String ValorCodigoBarra) throws SQLException{
        conectar conectar1 = new conectar();
        Connection connection1 = conectar1.conexion();
        String sql = "";
        sql = "INSERT INTO productos (idProductos, DescripcionProducto, ClaveProducto, CodigoBarras) VALUES (?,?,?,?)";
        try {
            PreparedStatement pst = connection1.prepareStatement(sql);
                pst.setString(1, Integer.toString(ValidProducto));
                pst.setString(2, ValDescripcion);
                pst.setString(3, ValClaveP);
                pst.setString(4, ValorCodigoBarra);
                int n = pst.executeUpdate();
                pst.close();
        if (n>0)
            JOptionPane.showMessageDialog(null, "Registro Exitoso");
        }
        catch (SQLException ex) {
            Logger.getLogger(NuevoProductos.class.getName()).log(Level.SEVERE, null, ex);
        }   
    }
        
            public void setActualizarProducto(int valorId, String valorDescripcion,String valorClave, String valorCodigo){
        conectar cn = new conectar();
        Connection con = cn.conexion(); 
       
        String sql = "UPDATE productos "+
                "SET DescripcionProducto = ?,"+
                "ClaveProducto = ?, "+
                "CodigoBarras = ? "+
                "WHERE idProductos = ?";
              
        try {
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, valorDescripcion);
            pst.setString(2, valorClave);
            pst.setString(3, valorCodigo);
            pst.setString(4, Integer.toString(valorId));
            int n = pst.executeUpdate();
            pst.close();
            if(n > 0)
            JOptionPane.showMessageDialog(null, "Actualización Exitosa");
        } catch (SQLException ex) {
            Logger.getLogger(ActualizarProductos.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
        
    public void setEliminarProducto(int valorId){
        conectar cn = new conectar();
        Connection con = cn.conexion();
        String sql = "DELETE FROM productos WHERE idProductos = ? ";
        try {
            PreparedStatement pst = con.prepareStatement(sql);
            pst.setString(1, Integer.toString(valorId));
            pst.execute();
            pst.close();
        } catch (SQLException ex) {
            Logger.getLogger(EliminarProductos.class.getName()).log(Level.SEVERE, null, ex);
        }   
    }
    
    
}


