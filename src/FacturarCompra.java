import java.awt.Color;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;


public class FacturarCompra extends javax.swing.JFrame {
    public static int banderaCompra;
    public static int banderaProveedorCompra;    
    Double subTotal, Grantotal, SubTotalModificar;

    /**
     * Creates new form FacturaCompra1
     */
    public FacturarCompra() {
        initComponents();
        getContentPane().setBackground(Color.white);
        setTitle("Factura de Compras");
        setResizable(false);
        setLocationRelativeTo(null);
        limpiar();
        bloquear();
        jTextFieldCodigoBarras.setEnabled(false);
        jButtonBuscarProducto.setEnabled(false);
        setCargarFecha();
        setCargarNumeroFactura();
        jTextFieldIdProveedor.requestFocus();
        subTotal=0.0;
        Grantotal=0.0;
        banderaCompra=0;
        banderaProveedorCompra=0;    
    }
    
    public void setBuscarProveedores(){
    boolean encontro = false;
    int valorIdProveedor = Integer.parseInt(jTextFieldIdProveedor.getText());
    conectar conectar1 = new conectar();
    Connection Connection1 = conectar1.conexion();
    
    String sql = "SELECT * FROM proveedores WHERE idProveedor = '"+valorIdProveedor+"'";
    
        try {
            Statement st = Connection1.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                encontro=true;
                jTextFieldNombre.setText(rs.getString(2)+""+rs.getString(3));
                jTextFieldCodigoBarras.setEnabled(true);
                jButtonBuscarProducto.setEnabled(true);
                jTextFieldCodigoBarras.requestFocus();
            }
            if (encontro==false) {
                //Metodo de seleccion de confirmacion
            int x = JOptionPane.showConfirmDialog(null, "El proveedor con id"+ valorIdProveedor);
            if (x== JOptionPane.YES_OPTION) {
                banderaProveedorCompra = 1;
                NuevoProveedorCompraVenta.valor = jTextFieldIdProveedor.getText();
                NuevoProveedorCompraVenta ncl = new NuevoProveedorCompraVenta();
                ncl.setVisible(true);
                dispose();
            }
            else if(x== JOptionPane.NO_OPTION){
                JOptionPane.showMessageDialog(null, "No es posible facturar si no es proveedor");
                jTextFieldIdProveedor.requestFocus();
            }
           }
            

        } catch (Exception ex) {
            JOptionPane.showConfirmDialog(null, ex);
        }    
    }
    
    public void setCargarFecha(){
    Date now = new Date (System.currentTimeMillis());
    SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
    // SimpleDateFormat hour = new SimpleDateFormat("HH:mm:ss")
    jTextFieldFecha.setText(date.format(now));
    //System.out.println(hour.format(now));
    //System.out.println(now);
    }
    
    public void setCargarNumeroFactura(){
        int maxNumFactura=0;
        conectar cn = new conectar();
        Connection con = cn.conexion();
        String sql = "SELECT MAX(idFacturasC) FROM facturascompra";
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while(rs.next()){
                if (rs.getString(1)!= null) {
                    maxNumFactura = Integer.valueOf(rs.getString(1).toString());
                } else {
                    maxNumFactura=0;
                }
            }
            maxNumFactura = maxNumFactura+1;
            jTextFieldFacturadeCompra.setText(String.valueOf(maxNumFactura));
        } catch (Exception ex) {
            JOptionPane.showConfirmDialog(null, ex);
        }
    }
    
public void desbloquear(){
// jButtonBuscarProducto.setEnabled(true);
jTextFieldCodigoBarras.setEnabled(true);
jTextFieldIdProducto.setEnabled(true);
jTextFieldDescripcionProducto.setEnabled(true);
jTextFieldValorUnitario.setEnabled(true);
jTextFieldClaveProducto.setEnabled(true);
jTextFieldCantidad.setEnabled(true);
jTextFieldTotalVenta.setEnabled(true);
jButtonAdiccionar.setEnabled(true);

}

public void bloquear(){
// jButtonBuscarProducto.setEnabled(false);
jTextFieldCodigoBarras.setEnabled(true);
jTextFieldIdProducto.setEnabled(false);
jTextFieldDescripcionProducto.setEnabled(false);
jTextFieldValorUnitario.setEnabled(false);
jTextFieldClaveProducto.setEnabled(false);
jTextFieldCantidad.setEnabled(false);
jTextFieldTotalVenta.setEnabled(false);
jButtonAdiccionar.setEnabled(false);
}

public void limpiar(){
//jButtonNuevo.setEnabled(true);
jTextFieldCodigoBarras.setText("");
jTextFieldIdProducto.setText("");
jTextFieldDescripcionProducto.setText("");
jTextFieldValorUnitario.setText("");
jTextFieldCantidad.setText("");
jTextFieldClaveProducto.setText("");
jTextFieldTotalVenta.setText("");
}

public void setGuardar(){
       DefaultTableModel model = (DefaultTableModel) jTableCompras.getModel();
       conectar conectar1 = new conectar();
       Connection connection1 = conectar1.conexion();
        String validProducto,valCantidad, valUni, valTotal;
            String sql = "";
            String sql2 = "";
            String sql3 = "";
            //---------------inicia ventas
            sql = "INSERT INTO compras (idCompra, FechaCompra, MontoCompra,proveedores_idproveedores) VALUES (?,?,?,?)";
                try {
                    PreparedStatement pst = connection1.prepareStatement(sql);
                    pst.setString(1, jTextFieldFacturadeCompra.getText());
                    pst.setString(2, jTextFieldFecha.getText() );
                    pst.setString(3, jTextFieldGranTotal.getText());
                    pst.setString(4, jTextFieldIdProveedor.getText());
                    
                    int n = pst.executeUpdate();
                    /*if(n>0)
                    JOptionPane.showMessageDialog(null, "registro exitoso ventas");*/
                } catch (SQLException ex) {
                    Logger.getLogger(FacturarCompra.class.getName()).log(Level.SEVERE, null, ex);
                }
            //------------------termina ventas
                //---------------inicia facturas
            sql = "INSERT INTO facturascompra (idFacturasC, FechaFacturaC, MontoFacturaC,Compras_idCompra,proveedores_idProveedor) VALUES (?,?,?,?,?)";
                try {
                    PreparedStatement pst = connection1.prepareStatement(sql);
                    pst.setString(1, jTextFieldFacturadeCompra.getText());
                    pst.setString(2, jTextFieldFecha.getText() );
                    pst.setString(3, jTextFieldGranTotal.getText());
                    pst.setString(4, jTextFieldFacturadeCompra.getText());
                     pst.setString(5, jTextFieldIdProveedor.getText());
                    
                    int n = pst.executeUpdate();
                   /* if(n>0)
                    JOptionPane.showMessageDialog(null, "registro exitoso Facturas");*/
                } catch (SQLException ex) {
                    Logger.getLogger(FacturarCompra.class.getName()).log(Level.SEVERE, null, ex);
                }
            //------------------termina facturas
            //-------------------inicio detalleventas
            int col = jTableCompras.getRowCount();
            for(int i=0; i<col; i++){
                    
                    validProducto = model.getValueAt( i, 1).toString();
                    valCantidad = model.getValueAt(i , 4).toString();
                    valUni = model.getValueAt(i , 5).toString();
                    valTotal = model.getValueAt(i , 6).toString();
                sql = "INSERT INTO detallecompras (compras_idCompras,productos_idProductos,CantidadProducto,ValorUnitario, subtotal) VALUES (?,?,?,?,?)";
                try {
                    PreparedStatement pst = connection1.prepareStatement(sql);
                    pst.setString(1, jTextFieldFacturadeCompra.getText());
                    pst.setString(2, validProducto);
                    pst.setString(3, valCantidad);
                    pst.setString(4, valUni);
                    pst.setString(5, valTotal);
                    
                    int n = pst.executeUpdate();
                   /* if(n>0)
                    JOptionPane.showMessageDialog(null, "registro exitoso detalle");*/
                } catch (SQLException ex) {
                    Logger.getLogger(FacturarCompra.class.getName()).log(Level.SEVERE, null, ex);
                }
                
            }
                 
         //..............
         int numfilas = jTableCompras.getRowCount();
         if(numfilas>=1){
            for(int i= numfilas-1; i>=0; i--){
                 model.removeRow(i);
             }        
          }
        Grantotal = 0.0;
        jTextFieldGranTotal.setText("");
        setCargarNumeroFactura();
        jTextFieldIdProveedor.setText("");
        jTextFieldNombre.setText("");
        bloquear();
        JOptionPane.showMessageDialog(null,"Registros guardados con éxito");
        jTextFieldIdProveedor.requestFocus();
        jTextFieldCodigoBarras.setEnabled(false);
   }

public void adicionar(){
        DefaultTableModel model = (DefaultTableModel) jTableCompras.getModel();
        if(!jTextFieldDescripcionProducto.getText().equals("")){
           model.addRow(new Object[]{jTextFieldCodigoBarras.getText(),
               jTextFieldIdProducto.getText(),jTextFieldDescripcionProducto.getText(),
               jTextFieldClaveProducto.getText(),jTextFieldCantidad.getText(), jTextFieldValorUnitario.getText(),
               jTextFieldTotalVenta.getText()
            } );
           subTotal = Double.parseDouble(jTextFieldTotalVenta.getText());
           Grantotal = Grantotal+subTotal;
           jTextFieldGranTotal.setText(Double.toString(Grantotal));
        }else{
            JOptionPane.showMessageDialog(null,"Seleccione Productos para adicionarlos a la factura");
        }
      
   }

public void eliminarTodo(){
   DefaultTableModel model = (DefaultTableModel) jTableCompras.getModel();
      int numfilas = jTableCompras.getRowCount();
      if(numfilas>=1){
          for(int i= numfilas-1; i>=0; i--){
              model.removeRow(i);
          }
          Grantotal = 0.0;
          jTextFieldGranTotal.setText(Double.toString(Grantotal));
      }else{
          JOptionPane.showMessageDialog(null,"No hay productos en la factura");
      }
      bloquear();
   }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jTextFieldNombre = new javax.swing.JTextField();
        jTextFieldFacturadeCompra = new javax.swing.JTextField();
        jTextFieldGranTotal = new javax.swing.JTextField();
        jTextFieldFecha = new javax.swing.JTextField();
        jTextFieldIdProveedor = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jButtonBuscarProducto = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        jTextFieldCodigoBarras = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jTextFieldIdProducto = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jTextFieldDescripcionProducto = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        jTextFieldClaveProducto = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        jTextFieldCantidad = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jTextFieldValorUnitario = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jTextFieldTotalVenta = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jButtonGuardar = new javax.swing.JButton();
        jButtonEliminarArticulo = new javax.swing.JButton();
        jButtonEliminarTodos = new javax.swing.JButton();
        jButtonRegresar = new javax.swing.JButton();
        jButtonAdiccionar = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableCompras = new javax.swing.JTable();
        jPanel4 = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(168, 37, 104));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("ID Proveedor");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Fecha");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Factura de Compra #");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(255, 255, 255));
        jLabel4.setText("Nombre");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Total");

        jTextFieldFacturadeCompra.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTextFieldFacturadeCompraMouseClicked(evt);
            }
        });

        jTextFieldIdProveedor.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextFieldIdProveedorFocusLost(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(37, 37, 37)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel1)
                    .addComponent(jLabel4))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jTextFieldNombre, javax.swing.GroupLayout.DEFAULT_SIZE, 168, Short.MAX_VALUE)
                    .addComponent(jTextFieldIdProveedor))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 58, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(42, 42, 42)
                        .addComponent(jTextFieldFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(90, 90, 90)
                        .addComponent(jLabel3))
                    .addComponent(jLabel5))
                .addGap(64, 64, 64)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTextFieldFacturadeCompra, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldGranTotal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(34, 34, 34))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(17, 17, 17)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3)
                    .addComponent(jTextFieldFacturadeCompra, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldFecha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldIdProveedor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(jTextFieldNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(25, 25, 25))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(jTextFieldGranTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        jButtonBuscarProducto.setText("Buscar");
        jButtonBuscarProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonBuscarProductoActionPerformed(evt);
            }
        });

        jLabel6.setText("Cod Barras");

        jTextFieldCodigoBarras.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextFieldCodigoBarrasFocusLost(evt);
            }
        });

        jLabel7.setText("IdProducto");

        jLabel8.setText("Descripcion");

        jLabel9.setText("Clave Producto");

        jLabel10.setText("Cantidad");

        jLabel11.setText("Valor/Unitario");

        jTextFieldValorUnitario.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextFieldValorUnitarioFocusLost(evt);
            }
        });

        jLabel12.setText("Total");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jButtonBuscarProducto)
                        .addGap(18, 18, 18)
                        .addComponent(jTextFieldCodigoBarras, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldIdProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 97, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldDescripcionProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(116, 116, 116)
                        .addComponent(jLabel6)
                        .addGap(53, 53, 53)
                        .addComponent(jLabel7)
                        .addGap(76, 76, 76)
                        .addComponent(jLabel8)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jTextFieldClaveProducto, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 82, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldValorUnitario, javax.swing.GroupLayout.PREFERRED_SIZE, 98, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldTotalVenta, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(20, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addGap(53, 53, 53)
                        .addComponent(jLabel10)
                        .addGap(38, 38, 38)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel12)
                        .addGap(43, 43, 43))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7)
                    .addComponent(jLabel8)
                    .addComponent(jLabel9)
                    .addComponent(jLabel10)
                    .addComponent(jLabel11)
                    .addComponent(jLabel12))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jTextFieldCodigoBarras, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldIdProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldDescripcionProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButtonBuscarProducto)
                    .addComponent(jTextFieldClaveProducto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldValorUnitario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldTotalVenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(12, 12, 12))
        );

        jButtonGuardar.setBackground(new java.awt.Color(102, 255, 102));
        jButtonGuardar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jButtonGuardar.setText("GUARDAR");
        jButtonGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonGuardarActionPerformed(evt);
            }
        });

        jButtonEliminarArticulo.setText("Eliminar Articulo");

        jButtonEliminarTodos.setText("Eliminar Todos");

        jButtonRegresar.setText("Regresar");

        jButtonAdiccionar.setText("+");
        jButtonAdiccionar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonAdiccionarActionPerformed(evt);
            }
        });
        jButtonAdiccionar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButtonAdiccionarKeyPressed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(35, 35, 35)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButtonEliminarArticulo, javax.swing.GroupLayout.DEFAULT_SIZE, 157, Short.MAX_VALUE)
                    .addComponent(jButtonGuardar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonEliminarTodos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonRegresar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jButtonAdiccionar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(28, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addComponent(jButtonAdiccionar, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jButtonGuardar)
                .addGap(18, 18, 18)
                .addComponent(jButtonEliminarArticulo)
                .addGap(18, 18, 18)
                .addComponent(jButtonEliminarTodos)
                .addGap(18, 18, 18)
                .addComponent(jButtonRegresar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTableCompras.setBackground(new java.awt.Color(204, 204, 204));
        jTableCompras.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
        jTableCompras.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jTableCompras.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Cod", "Id Producto", "Descripcion", "Clave", "Cantidad", "Valor/Unitario", "ValorTotal"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class, java.lang.String.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
        jTableCompras.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableComprasMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTableCompras);

        jPanel4.setBackground(new java.awt.Color(168, 37, 104));

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 155, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 0, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 863, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(14, 14, 14)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextFieldFacturadeCompraMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTextFieldFacturadeCompraMouseClicked
        // TODO add your handling code here:
        FacturarCompra obj = new FacturarCompra();
        obj.setVisible(true);
        dispose();
    }//GEN-LAST:event_jTextFieldFacturadeCompraMouseClicked

    private void jTextFieldIdProveedorFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldIdProveedorFocusLost
        // TODO add your handling code here:
        if (jTextFieldIdProveedor.getText().equals("")) {
            jTextFieldIdProveedor.requestFocus();
        } else {
            setBuscarProveedores();
        }
    }//GEN-LAST:event_jTextFieldIdProveedorFocusLost

    private void jButtonBuscarProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonBuscarProductoActionPerformed
        // TODO add your handling code here:
        banderaCompra = 1;
        ConsultarProductoCompraVenta obj = new ConsultarProductoCompraVenta();
        obj.setVisible(true);
    }//GEN-LAST:event_jButtonBuscarProductoActionPerformed

    private void jTextFieldCodigoBarrasFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldCodigoBarrasFocusLost
        // TODO add your handling code here:
        if (jTextFieldCodigoBarras.getText().equals("")) {
        } else {
            boolean igual = false;//El producto no existe
            String registro [] = new String[1];
            String valorCodigoBarras = jTextFieldCodigoBarras.getText();
            String sql = "SELECT * FROM productos WHERE CodigoBarras like '"+valorCodigoBarras+"'";
            conectar conectar1 = new conectar();
            Connection cn = conectar1.conexion();
                try {
                    Statement st = cn.createStatement();
                    ResultSet rs = st.executeQuery(sql);
                        while (rs.next()){
                        igual = true;
                        registro[0]= rs.getString("CodigoBarras");
                        jTextFieldIdProducto.setText(rs.getString("idProductos"));
                        jTextFieldDescripcionProducto.setText(rs.getString("DescripcionProducto"));
                        jTextFieldClaveProducto.setText(rs.getString("ClaveProducto"));
                        jTextFieldCantidad.requestFocus();
                        desbloquear();
                       }
                        if (igual == false){
                        //JOptionPane.showMessageDialog(null,"¿Producto no existe desea registrarlo en el inventario?");
                        int x = JOptionPane.showConfirmDialog(null, "El producto no existe, desea registrarlo");
                            if (x == JOptionPane.YES_OPTION){
                            NuevoProductoComprar obj = new NuevoProductoComprar();
                            obj.setVisible(true);
                    }
                                else if(x == JOptionPane.NO_OPTION){
                                JOptionPane.showMessageDialog(null, "No es posible facturar si el producto no está en el sistema");
                                limpiar();
                                bloquear();
                                jTextFieldCodigoBarras.requestFocus();
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(FacturarCompra.class.getName()).log(Level.SEVERE, null, ex);
    }
}
    }//GEN-LAST:event_jTextFieldCodigoBarrasFocusLost

    private void jTextFieldValorUnitarioFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldValorUnitarioFocusLost
        // TODO add your handling code here:
        double cantidad = Double.parseDouble(jTextFieldCantidad.getText());
        double valorUnitario = Double.parseDouble(jTextFieldValorUnitario.getText());
        double total = cantidad*valorUnitario;
        jButtonAdiccionar.requestFocus();
        jTextFieldTotalVenta.setText(Double.toString(total));
    }//GEN-LAST:event_jTextFieldValorUnitarioFocusLost

    private void jTableComprasMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableComprasMouseClicked
        jButtonAdiccionar.setEnabled(false);
            DefaultTableModel model = (DefaultTableModel) jTableCompras.getModel();

            SubTotalModificar = 0.0;
            //SubTotalModificar = Double.parseDouble(jTextFieldTotalVenta.getText());
            SubTotalModificar = Double.parseDouble(model.getValueAt(jTableCompras.getSelectedRow(), 6).toString());

        // TODO add your handling code here:
    }//GEN-LAST:event_jTableComprasMouseClicked

    private void jButtonAdiccionarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonAdiccionarActionPerformed
        adicionar();
        limpiar();
        bloquear();
        jTextFieldCodigoBarras.requestFocus();

        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonAdiccionarActionPerformed

    private void jButtonAdiccionarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButtonAdiccionarKeyPressed
        adicionar();
        limpiar();
        bloquear();
        jTextFieldCodigoBarras.requestFocus();        
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonAdiccionarKeyPressed

    private void jButtonGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonGuardarActionPerformed
        // TODO add your handling code here:
                if (jTableCompras.getRowCount()==0) {
            JOptionPane.showMessageDialog(null,"Ingrese productos a la factura");
            jTextFieldCodigoBarras.requestFocus();
        } else {
             setGuardar();
        }
    }//GEN-LAST:event_jButtonGuardarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FacturarCompra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FacturarCompra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FacturarCompra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FacturarCompra.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FacturarCompra().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonAdiccionar;
    private javax.swing.JButton jButtonBuscarProducto;
    private javax.swing.JButton jButtonEliminarArticulo;
    private javax.swing.JButton jButtonEliminarTodos;
    private javax.swing.JButton jButtonGuardar;
    private javax.swing.JButton jButtonRegresar;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jTableCompras;
    private javax.swing.JTextField jTextFieldCantidad;
    private javax.swing.JTextField jTextFieldClaveProducto;
    public static javax.swing.JTextField jTextFieldCodigoBarras;
    private javax.swing.JTextField jTextFieldDescripcionProducto;
    private javax.swing.JTextField jTextFieldFacturadeCompra;
    private javax.swing.JTextField jTextFieldFecha;
    private javax.swing.JTextField jTextFieldGranTotal;
    private javax.swing.JTextField jTextFieldIdProducto;
    public static javax.swing.JTextField jTextFieldIdProveedor;
    private javax.swing.JTextField jTextFieldNombre;
    private javax.swing.JTextField jTextFieldTotalVenta;
    private javax.swing.JTextField jTextFieldValorUnitario;
    // End of variables declaration//GEN-END:variables
}
