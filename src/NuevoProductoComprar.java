
import java.awt.Color;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Diego Fernando
 */
public class NuevoProductoComprar extends javax.swing.JFrame {
    int ProductoExistente = 0;
    /**
     * Creates new form GestionarClientes
     */
    public NuevoProductoComprar() {
        initComponents();
        setResizable(false);
        setLocation(0,0);
        setTitle("Nuevo Producto"); 
        getContentPane().setBackground(Color.white);
        jButtonGuardar.setEnabled(false);
    }
   public void setValidarIdProduct(){
        ProductoExistente = 0;
       String [] registros = new String[1];
       String valor = jTextFieldIdProducto.getText();
       String sql = "SELECT * FROM productos where idProductos LIKE'%"+valor+"%'";
     
       conectar conectar1 = new conectar();
       Connection Connection1 = conectar1.conexion();
       
        try {
        Statement st = Connection1.createStatement();
        ResultSet rs = st.executeQuery(sql);
        while (rs.next()){
            ProductoExistente = 1;
        }
      
        } catch (SQLException ex) {
            JOptionPane.showConfirmDialog(null, ex);
        }
        if (ProductoExistente == 1) {
            JOptionPane.showMessageDialog(null,"El Producto ya existe");
            jTextFieldIdProducto.requestFocus();
            jTextFieldIdProducto.setText("");
        } else {
            
            jButtonGuardar.setEnabled(true);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jTextFieldDescripcion = new javax.swing.JTextField();
        jTextFieldClavedeProducto = new javax.swing.JTextField();
        jButtonGuardar = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jTextFieldIdProducto = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jTextFieldCodigodeBarras = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(1367, 740));
        getContentPane().setLayout(null);

        jLabel.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel.setText("Descripcion");
        getContentPane().add(jLabel);
        jLabel.setBounds(290, 370, 70, 20);

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel2.setText("Clave Producto");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(250, 400, 110, 20);
        getContentPane().add(jTextFieldDescripcion);
        jTextFieldDescripcion.setBounds(380, 360, 250, 30);
        getContentPane().add(jTextFieldClavedeProducto);
        jTextFieldClavedeProducto.setBounds(380, 390, 250, 30);

        jButtonGuardar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jButtonGuardar.setText("Guardar");
        jButtonGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonGuardarActionPerformed(evt);
            }
        });
        jButtonGuardar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jButtonGuardarKeyPressed(evt);
            }
        });
        getContentPane().add(jButtonGuardar);
        jButtonGuardar.setBounds(380, 480, 250, 33);

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel3.setText("Id Producto");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(250, 340, 110, 20);

        jTextFieldIdProducto.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                jTextFieldIdProductoFocusLost(evt);
            }
        });
        jTextFieldIdProducto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldIdProductoActionPerformed(evt);
            }
        });
        getContentPane().add(jTextFieldIdProducto);
        jTextFieldIdProducto.setBounds(380, 330, 250, 30);

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel5.setText("Codigo de Barras");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(240, 430, 120, 20);
        getContentPane().add(jTextFieldCodigodeBarras);
        jTextFieldCodigodeBarras.setBounds(380, 420, 250, 30);

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Lateral 2.jpeg"))); // NOI18N
        getContentPane().add(jLabel4);
        jLabel4.setBounds(240, 290, 450, 310);

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/img/Proveedores 2.jpeg"))); // NOI18N
        jLabel8.setText("jLabel1");
        getContentPane().add(jLabel8);
        jLabel8.setBounds(170, 0, 1230, 870);

        jPanel1.setBackground(new java.awt.Color(168, 37, 104));
        jPanel1.setLayout(null);
        getContentPane().add(jPanel1);
        jPanel1.setBounds(0, 0, 170, 690);

        jMenu1.setText("Salir");
        jMenu1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jMenu1MousePressed(evt);
            }
        });
        jMenuBar1.add(jMenu1);

        jMenu2.setText("Regresar");
        jMenu2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenu2MouseClicked(evt);
            }
        });
        jMenuBar1.add(jMenu2);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButtonGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonGuardarActionPerformed
        setValidarIdProduct();
        if(ProductoExistente == 0){
        try {
            // TODO add your handling code here:
            Productos Productos1 = new Productos();
            Productos1.setIdProducto(Integer.parseInt(jTextFieldIdProducto.getText()));
            Productos1.setDescripcion(jTextFieldDescripcion.getText());
            Productos1.setClaveP(jTextFieldClavedeProducto.getText());
            Productos1.setCodigoBarra(jTextFieldCodigodeBarras.getText());
            Productos1.setInsertarProducto(Productos1.getIdProducto(), Productos1.getDescripcion(), Productos1.getClaveP(), Productos1.getCodigoBarra());
        } catch (SQLException ex) {
            Logger.getLogger(NuevoProductos.class.getName()).log(Level.SEVERE, null, ex);
        }
        jTextFieldIdProducto.setText("");
        jTextFieldDescripcion.setText("");
        jTextFieldClavedeProducto.setText("");
        jTextFieldCodigodeBarras.setText("");
        jTextFieldIdProducto.requestFocus();
        jButtonGuardar.setEnabled(false);
        }
    }//GEN-LAST:event_jButtonGuardarActionPerformed

    private void jMenu1MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu1MousePressed
        // TODO add your handling code here:
         int x = JOptionPane.showConfirmDialog(null, "¿Desea salir del programa?");
                    if (x == JOptionPane.YES_OPTION){
                        System.exit(0);
                    }
//                    else if(x == JOptionPane.NO_OPTION){
//                    }
    }//GEN-LAST:event_jMenu1MousePressed

    private void jMenu2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenu2MouseClicked
        // TODO add your handling code here:
        GestionarProductos gc1 = new GestionarProductos();
        gc1.setVisible(true);
        dispose();
    }//GEN-LAST:event_jMenu2MouseClicked

    private void jButtonGuardarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jButtonGuardarKeyPressed
        setValidarIdProduct();
        if(ProductoExistente == 0){
        try {
            // TODO add your handling code here:
           Productos Productos1 = new Productos();
            Productos1.setIdProducto(Integer.parseInt(jTextFieldIdProducto.getText()));
            Productos1.setDescripcion(jTextFieldDescripcion.getText());
            Productos1.setClaveP(jTextFieldClavedeProducto.getText());
            Productos1.setCodigoBarra(jTextFieldCodigodeBarras.getText());
            Productos1.setInsertarProducto(Productos1.getIdProducto(), Productos1.getDescripcion(), Productos1.getClaveP(), Productos1.getCodigoBarra());
        } catch (SQLException ex) {
            Logger.getLogger(NuevoProductos.class.getName()).log(Level.SEVERE, null, ex);
        }
        jTextFieldIdProducto.setText("");
        jTextFieldDescripcion.setText("");
        jTextFieldClavedeProducto.setText("");
        jTextFieldCodigodeBarras.setText("");
        jTextFieldIdProducto.requestFocus();
        jButtonGuardar.setEnabled(false);
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonGuardarKeyPressed

    private void jTextFieldIdProductoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_jTextFieldIdProductoFocusLost
       
        if (jTextFieldIdProducto.getText().equals("")) {
            
        } else {
             setValidarIdProduct();           
        }
       

        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldIdProductoFocusLost

    private void jTextFieldIdProductoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldIdProductoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldIdProductoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(NuevoCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(NuevoCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(NuevoCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(NuevoCliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new NuevoProductos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonGuardar;
    private javax.swing.JLabel jLabel;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jTextFieldClavedeProducto;
    private javax.swing.JTextField jTextFieldCodigodeBarras;
    private javax.swing.JTextField jTextFieldDescripcion;
    private javax.swing.JTextField jTextFieldIdProducto;
    // End of variables declaration//GEN-END:variables
}
